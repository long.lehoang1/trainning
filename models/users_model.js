const path = require('path');

const Knex = require('knex');
const { Model } = require('objection');

const knex = Knex({
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: {
        filename: path.join(__dirname, '../', 'coursetrain.db'),
    }
});
Model.knex(knex);

class Answer extends Model {
    static get tableName() {
        return 'answers';
    }

    static get relationMappings() {
        return {
            questions: {
                relation: Model.HasManyRelation,
                modelClass: Answer,
                join: {
                    from: 'questions.id',
                    to: 'answers.questionId'
                }
            }
        }
    }
}
class Question extends Model {
    static get tableName() {
        return 'questions';
    }

    static get relationMappings() {
        return {
            answers: {
                relation: Model.HasManyRelation,
                modelClass: Answer,
                join: {
                    from: 'questions.id',
                    to: 'answers.questionId'
                }
            }
        };
    }
}
class Course extends Model {
    static get tableName() {
        return 'courses';
    }

    static get relationMappings() {
        return {}
    }
}
class User extends Model {
    static get tableName() {
        return 'users';
    }

    static get relationMappings() {
        return {
            userCourses: {
                relation: Model.ManyToManyRelation,
                modelClass: Course,
                join: {
                    from: 'users.id',
                    through: {
                        from: 'users_courses.userId',
                        to: 'users_courses.courseId',
                        extra: ['status']
                    },
                    to: 'courses.id'
                }
            }
        }
    }
}

class Command {
    async getCourses(username) {
        const allCourses = await Course.query().select();

        const { userCourses } = (await User.query()
            .findOne({ username })
            .eager('userCourses')) || { userCourses: [] };

        userCourses.map((userCourse) => {
            const attemptedCourse = allCourses.find((course) => course.id === userCourse.id);
            attemptedCourse.status = userCourse.status;
        });

        return allCourses;
    }

    async createUser(username, password) {
        const user = await User
            .query()
            .findOne({
                username,
                password,
            });


        if (user) return;

        return await User.query()
            .insert({
                username,
                password,
            });
    }

    async getQuiz(courseId) {
        console.log("fdfcwe")
        return await Question.query()
            .where('courseId', courseId)
            .eager('answers');
    }

    async getAnswers(questionId){
        return await Answer.query()
        .where('questionId', questionId)

    }
}

const command = new Command();

// export default command;
module.exports = command;
