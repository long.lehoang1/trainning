const { app, BrowserWindow, ipcMain } = require("electron")
const path = require('path');
const url = require('url');

var knex = require("knex")({
    client: "sqlite3",
    connection: {
        filename: path.join(__dirname, 'coursetrain.db')
    }
});

app.on("ready", () => {
    let mainWindow = new BrowserWindow({ height: 800, width: 800, show: false })
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views/login.html'),
        protocol: 'file',
        slashes: true
    }));
    mainWindow.once("ready-to-show", () => { mainWindow.show() })

    // ipcMain.on('entry-accepted', (event, arg) => {
    //     if(arg=='ping'){
    //         win.show()
    //         child.hide()
    //     }
    //   });
    ipcMain.on("mainWindowLoaded", function () {
        let result = knex.select("username").from("users")
        result.then(function (rows) {
            mainWindow.webContents.send("resultSent", rows);
            
        })
        console.log('sdcfedr');
    });

});
