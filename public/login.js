let $ = require('jquery')
let fs = require('fs')
let filename = 'coursetrain'
const UserModel = require('../models/users_model.js');
const ipc = require('electron').ipcRenderer;
$('#btn-login').on('click', (e) => {

    const username = document.querySelector('[name="username"]').value;
    const password = document.querySelector('[name="password"]').value;

    if (fs.existsSync(filename)) {
        let data = fs.readFileSync(filename, 'utf8').split('\n')

        data.forEach((coursetrain, index) => {
            let [user, password] = coursetrain.split(',');
            let [name1, usr] = user.split(':')
            let [name2, pass] = password.split(':')
            let txtUser = $('#txtUsr').val();
            let txtPwd = $('#txtPwd').val();
            if (txtUser == usr && txtPwd == pass) {
                ipc.sendSync('mainWindowLoaded', 'abc')

            }
            else {
                $('#lbl').text('Username or Password is incorrect')
            }
        })
    }
    e.preventDefault();
})

